
## Codettes Bootcamp 2022-2023 Summary
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQaXW2arkKUCEmf-D-Or8d14qqySkfsaQH9z_IqRIZYW30uVnx-5Dn8xmNuikgRoEeSkSVI23v2rXhi/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

#### LINKS TO CODETTES BOOTCAMP STUDENTS 2022-2023 PORTFOLIO 
### [Keshav Haripersad](https://keshav11-coder.github.io/bootcamp2022/).
### [Shanika Doekhi](https://docs.google.com/document/d/1eglzp9VxP5ZhNhrQItTOkkPXQ5G0hqphaRtn_YdN-2Y/edit?usp=sharing ).
### [Varun Brahmadin](https://varun-brahm.github.io/codettesbootcamp2022/ ).
### [Dylan Vianen](https://ddylvianen.github.io/codettesbootcamp2022/).
### [Zahira Abdoel](https://sjazh.github.io/codettesbootcamp2022/).
### [Axl Redjopawiro](https://jumperboyax.github.io/coddettesbootcamp2022/).
### [Jenielva Shepherd](https://jenielva.github.io/ ).
### [Siemla Kuldop Singh]().
### [Arissa Nirmal](https://arissanirmal2007.github.io/).

## Codettes Bootcamp 2020 Summary
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSSKVpFX9DLn8b8aTvLBXrn2lemQ9eFmJzNMoE1wGhW4QUzc7EnHHMQ5WMUboo8yWpHByJ56RgQwr-g/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

#### LINKS TO CODETTES BOOTCAMP STUDENTS 2020 PORTFOLIO 
### [Devika Mahabir](https://codettes-boothcamp.gitlab.io/students/devika-mahabir ).
### [Shewiskha Ganpat](https://codettes-boothcamp.gitlab.io/students/suraisha-ganpat ).
### [Ivy Amatredjo](https://codettes-boothcamp.gitlab.io/students/ivy-amatredjo ).
### [Anjessa Linger](https://codettes-boothcamp.gitlab.io/students/anjessa-linger ).
### [Faith Pherai](https://codettes-boothcamp.gitlab.io/students/faith-pherai ).
### [Drishti Boeddha](https://codettes-boothcamp.gitlab.io/students/drishti-boeddha ).
### [Disha Boeddha](https://codettes-boothcamp.gitlab.io/students/disha-boeddha).
### [Shofia Notosoewito](https://codettes-boothcamp.gitlab.io/students/shofia-notosoewito).
### [Yael Cairo](https://codettes-boothcamp.gitlab.io/students/yael-cairo).
### [Darian Tedja](https://codettes-boothcamp.gitlab.io/students/darian-tedja).






