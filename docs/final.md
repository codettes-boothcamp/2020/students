#CODETTES BOOTCAMP CLASS 2022-2023

## Link to Online Portfolio 
[Keshav Haripersad](https://codettes-boothcamp.gitlab.io/students/devika-mahabir ).

## Keshav Haripersad  Final Project "SKYCAM"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSDn-aadVx44fym_XyCsQMgNlE3fW1a1zFHGhi16dHlxJyhIHWQM0FPVLElpxuiBBSer1mb_pUmRd2q/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Varun Brahmadin](https://varun-brahm.github.io/codettesbootcamp2022).

## Varun Brahmadin Final Project "SUBS"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTpas16obvXt0vXySv4pc0g-USXmBmoNIkafs-7c6wB2FqaXduQyf2CmeRvTcSTAZ_j0Csa03EA8AH3/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Dylan Vianen](https://ddylvianen.github.io/codettesbootcamp2022/).

## Dylan Vianen Final Project "Visionary Hands"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSHYHbBUXA7Dxww1MYV7J8J_A8W-SEJtRXQFgAH36f6e7hmtPyg1sTIwTyiCM9HzQ/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Zahira Abdoel](https://sjazh.github.io/codettesbootcamp2022/).

## Zahira Abdoel Final Project "SunTracker"
<Zahira Abdoel="https://docs.google.com/presentation/d/e/2PACX-1vTb0Ra-xtvhWBunoCLYw4o8pHs_YXGa5L7Cc-tnOV_i5D6T60pNHHuvj3yAjbBAiDUU3YCJNAnT8Ji8/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Axl Redjopawiro](https://jumperboyax.github.io/coddettesbootcamp2022/).

## Axl Redjopawiro Final Project "FARMBOT"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ9dLK32pjQrXkwZMWHlkeIe7LHGfVMKyyo3KbJMI6p5CKi12kJf1hgH9M-Q9Ku7-QJ4svdCtkX14JS/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Jenielva Shepherd](https://jenielva.github.io/).

## Jenielva Shepherd Project "SMART VISSION"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTAqQh_yo8eMKXQczCIyXYawF9AJUFOemtiR0zVi3wMmqxiEbLn1VdAmq4UVt61tTRI19_8TEu3DxiV/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Arissa Nirmal](https://arissanirmal2007.github.io/).

## Arissa Nirmal Project "DelMare"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQTNzYV-FeW6fO5jDOw7p9fY_K1QsuvvpaRMxNMQaoZdDSb0-6ffDD0cvUkySp0TigozhD87m-54oX2/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Shanika Doekhi ](https://docs.google.com/document/d/1eglzp9VxP5ZhNhrQItTOkkPXQ5G0hqphaRtn_YdN-2Y/edit?usp=sharing).


## Shanika Doekhi Project ""
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRVoxrdAXYoeD4Mfn1EhrZR15ee8WXKEcdyQj1T-bEenutbJpDcs4NgiSZtgwT_eEFM2ShrKfPI65Oc/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Siemla Kuldip Singh]().

## Siemla Kuldip Singh Project ""
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRVoxrdAXYoeD4Mfn1EhrZR15ee8WXKEcdyQj1T-bEenutbJpDcs4NgiSZtgwT_eEFM2ShrKfPI65Oc/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

#### HACKOMATION 2022 FINAL PROJECTS 

## Inno4"Ven02"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSGxVo9GxZlvE9pZ3Nyc3UQvGZ6ILENlZjDFQx5p246thAAurEX1ZWqHkXcm0vWayMHo8M9XUkLaYwO/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

##  "HackWhack"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSW2uLNblKwgx0gepF-vJJO1RElCjXjA5IDL3FO0AAGpHLqr4KkIioqiG9R6V5NQX_mbup0_95uRnS6/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Inno5 "SkyCam ""
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSafVZIDEg-eYe4UhU3oo94uxgluQUwwhcoD21I5yZCxzHPhhHWUDMUdZLnHNx7SGfXfgSZOde4Tq9y/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Cyberoptics"BoringCam ""
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT9o0hpgTF58-xr33Kp5d_vH36EzGWkTiTvUMc5LqpUPrxYb-WsjJ310Ki50CQKiWlUaIUspfmIQ7Rm/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


#### HACKOMATION 2020 FINAL PROJECTS 

## Innov8rs "TRIBE"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRK3FO4or5UAAY22daHH1ye1qtY8rXX98W_oMvpaiZakH3ngDwaGDYGc_T8cT5MexD8-jzKo_1OQZD7/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Leadhers "Profusu"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSCNglvwYzPL0d5qrGTHe18OC2gHVhabFao7UhUjN6xSFhuZV1W6VT5o07yGBSOhrS8uYmYU_U5T-RI/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


#CODETTES BOOTCAMP CLASS 2020-2021

## Devika Mahabir  Final Project "Scleraya"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQXMHvj2ag3X_EwOnfmFsQgEAMHdC_miCJf4_90LONEpXzpYlwbLfRCITIpFImZpRNs-SCjmO755xPj/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Devika Mahabir](https://codettes-boothcamp.gitlab.io/students/devika-mahabir ).

## Shewiskha Ganpat Final Project "SafetyWish"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRHUe14pIo4ZCjmkqJgJyUx1WQevac4iAyfDGueNsL97pAwMddzZxAX5VqXxXLim-mMaTy87-TVtYb1/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Shewiskha Ganpat](https://codettes-boothcamp.gitlab.io/students/suraisha-ganpat ).

## Ivy Amatredjo Final Project "Speaking Panda"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRV63vIDDio_T55iJYfb-3VINA3KosUfYnsmgLfZD3lojT0OEG706oPuz7q3iytr1oaExl4LJmXs_7K/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Ivy Amatredjo](https://codettes-boothcamp.gitlab.io/students/ivy-amatredjo ).

## Anjessa Linger Final Project "Ankila"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTb0Ra-xtvhWBunoCLYw4o8pHs_YXGa5L7Cc-tnOV_i5D6T60pNHHuvj3yAjbBAiDUU3YCJNAnT8Ji8/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Anjessa Linger](https://codettes-boothcamp.gitlab.io/students/anjessa-linger ).

## Faith Pherai Final Project "Linola"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRLGl4yOhZqoixF6LP_mfy87IwgTZzGu7aA7vdlAd-T7QvJbTPz1Wi1Ye3i7cH2wUWYi_hqwHSGY3T2/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Faith Pherai](https://codettes-boothcamp.gitlab.io/students/faith-pherai ).

## Drishti Boeddha Project "Robocleaner"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTNJCKsWidT8GIq2OXrkGQMcfITUR8-WGLFEgg_ErC1UkGku7WXWjrmEWZb-KVmXiJfc83g4DOsD7it/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Drishti Boeddha](https://codettes-boothcamp.gitlab.io/students/drishti-boeddha ).

## Disha Boeddha Project "Smart Nature"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTf0RbZbYNz_XDOWzUsRr_AQgQ-fNrU2fOIPTw7N6UhzmgcBIoNCTBE_La-2DG8kZKb7N_xgKuUbe3a/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Disha Boeddha](https://codettes-boothcamp.gitlab.io/students/disha-boeddha).

## Shofia Notosoewito Project ""
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRVoxrdAXYoeD4Mfn1EhrZR15ee8WXKEcdyQj1T-bEenutbJpDcs4NgiSZtgwT_eEFM2ShrKfPI65Oc/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Shofia Notosoewito](https://codettes-boothcamp.gitlab.io/students/shofia-notosoewito).

## Yael Cairo Project ""
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT83BYjQ0v-to7-AnSidxrQDYmZFoTRtaPPMiTOUM27VM2F3JM8D-E-NakZqb4fTbVzRgXDucIKEZdI/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Link to Online Portfolio 
[Yael Cairo](https://codettes-boothcamp.gitlab.io/students/yael-cairo).

## Darian Tedja Project ""
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR9xKKWSZJCBolxSvBgNZjFb9fWPDNc25_NlxGjS8uJhSQnUFhOdFeB16Dhtv6TI42ZcZlvh989395H/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe> 

## Link to Online Portfolio 
[Darian Tedja](https://codettes-boothcamp.gitlab.io/students/darian-tedja).


#### HACKOMATION 2020 FINAL PROJECTS 

## Innov8rs "TRIBE"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRK3FO4or5UAAY22daHH1ye1qtY8rXX98W_oMvpaiZakH3ngDwaGDYGc_T8cT5MexD8-jzKo_1OQZD7/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Leadhers "Profusu"
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSCNglvwYzPL0d5qrGTHe18OC2gHVhabFao7UhUjN6xSFhuZV1W6VT5o07yGBSOhrS8uYmYU_U5T-RI/embed?start=true&loop=true&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
